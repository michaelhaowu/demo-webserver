# Instructions

## Project preparation

1. Run the project by starting the node server - the port by default is 3001

```
npm i
npm start
```

2.  Start a new tab and navigate to `localhost:3001/vehicleData` to see the functional but incomplete page.

    Please expect to see a simple html page such as the following

    ![Baseline vehicleData page](./public/img/baseline_vehicleData.png)

3.  Run the react front end by starting the app - the port by default is 3000

```
cd ui
npm i
npm start
```

4.  Start a new tab and navigate to `localhost:3000/` to see the functional but incomplete react application.

    Please expect to see a simple page displaying the following

    ![Baseline react application page](./public/img/baseline_react.png)

### Static Site - Enable the buttons

1.  On `localhost:3001/vehicleData`, click the buttons `getVehicle` and `getGps` buttons to trigger the expected route.

    They will currently show the following statuses from the console.

    ![Baseline getVehicle console](./public/img/baseline_getVehicle_getGps.png)

2.  Change the code base such that the buttons respectively return the following response.

    ![Fixed getVehicle console](./public/img/solution_getVehicle.png)

    ![Fixed getVehicle console](./public/img/solution_getGps.png)

### Static Site - Collect GPS point for a single vehicle

1.  On `localhost:3001/vehicleData`, the button `getGpsByVehicle` is currently not returning the gps points for the specified vehicle.

    Please change the project such that it will display only the gps points of the requested vehicle in the text box. See example below.

    ![Fixed getGpsByVehicle request](./public/img/solution_getGpsByVehicle_request.png)

    ![Fixed getGpsByVehicle response](./public/img/solution_getGpsByVehicle_response.png)

### React App - Update the react to query and count the

1.  On the react app at `localhost:3000`, the button `Query` is currently not returning the number of gps point per the correct vehicle.

    Please change the project such that it will display only the gps points of the requested vehicle in the text box. See example below.

    Hint: the main changes should be within the `ui/src/App.js` file, where some existing react hook structures already exist.

    ![Fixed findVehicleGps empty](./public/img/solution_react_findVehicleGps_response_empty.png)

    ![Fixed findVehicleGps vehicle1](./public/img/solution_react_findVehicleGps_response_vehicle1.png)

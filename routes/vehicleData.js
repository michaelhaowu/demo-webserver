var express = require('express');
var router = express.Router();

const path = require('path');
const {client} = require('websocket');

const {DateTime} = require('luxon');

const dotenv = require('dotenv');
dotenv.config();

const faunadb = require('faunadb');
var q = faunadb.query;

const FDB_SECRET = process.env.FDB_SECRET;
const FDB_ACCESS_ROLE = 'server';
var FDB_CLIENT = new faunadb.Client({secret: FDB_SECRET});

// GPS Points cache
let GPS_POINTS = [];

const {
  Call,
  Paginate,
  Documents,
  Map,
  Collection,
  Lambda,
  Get,
  Var,
  CreateCollection,
  Create,
  Exists,
  Databases,
  Collections,
  Functions,
  Index,
  Intersection,
  Select,
  Indexes,
  Delete,
  Ref,
  Match,
  Update,
  Login,
  Range,
  Time
} = q;

/* GET users listing. */
router.get('/', function (req, res, next) {
  console.log('Sending: ', path.join(__dirname, '../public/html/vehicleData.html'));
  res.sendFile(path.join(__dirname, '../public/html/vehicleData.html'));
});

router.get('/getGps', async function (req, res, next) {
  var gps = await getGps();

  res.json({data: gps});
});

router.get('/getVehicles', async function (req, res, next) {
  var vehicles = await getVehicles();

  res.json({data: vehicles});
});

router.get('/getGpsByVehicles', async function (req, res, next) {
  if (typeof req.query.vehicleName !== 'undefined' && req.query.vehicleName != '') {
    const vehicleName = req.query.vehicleName;
    if (GPS_POINTS.length == 0) {
      GPS_POINTS = await getGps();
    }

    const vehicleGps = GPS_POINTS.filter((gpsPoint) => {
      return gpsPoint.data.vehicleName == vehicleName;
    });

    res.json({data: vehicleGps});
  } else {
    res.json({data: []});
  }
});

// Querying functions
async function getVehicles() {
  console.log('getVehicles');
  let hasAfter = false;

  const initialPage = await FDB_CLIENT.query(
    Map(Paginate(Documents(Collection('vehicles')), {size: 999}), Lambda('x', Get(Var('x'))))
  );
  const data = initialPage.data;
  let after;
  if (initialPage.after) {
    hasAfter = true;
    after = initialPage.after;
  }

  while (hasAfter == true) {
    const nextPage = await FDB_CLIENT.query(
      Map(Paginate(Documents(Collection('vehicles')), {size: 999, after: after}), Lambda('x', Get(Var('x'))))
    );
    if (!nextPage.after) {
      hasAfter = false;
    } else {
      after = nextPage.after;
    }
    data.push(...nextPage.data);
  }

  console.log('faunaVehicleResult', data);

  return data;
}

async function getGps() {
  console.log('getGps');

  let hasAfter = false;

  const initialPage = await FDB_CLIENT.query(
    Map(Paginate(Documents(Collection('gps')), {size: 999}), Lambda('x', Get(Var('x'))))
  );
  const data = initialPage.data;
  let after;
  if (initialPage.after) {
    hasAfter = true;
    after = initialPage.after;
  }

  while (hasAfter == true) {
    const nextPage = await FDB_CLIENT.query(
      Map(Paginate(Documents(Collection('gps')), {size: 999, after: after}), Lambda('x', Get(Var('x'))))
    );
    if (!nextPage.after) {
      hasAfter = false;
    } else {
      after = nextPage.after;
    }
    data.push(...nextPage.data);
  }

  console.log('faunaGpsResult', data);

  return data;
}

module.exports = router;
